// librarie to store and editing data

// Dependencies
const fs = require('fs')
const path = require('path')

// Container
const lib = {}
const root = path.join(__dirname, '/../.data/')

lib.create = (dir, file, data, callback) => {
  // open the file for waiting
  fs.open(
    `${root}${dir}/${file}.json`,
    'wx',
    (err, fileDescriptor) => {
      console.log(err, fileDescriptor)
      if (!err && fileDescriptor) {
        const writableData = JSON.stringify(data)
        fs.writeFile(
          fileDescriptor,
          writableData,
          err => {
            if (!err) {
              fs.close(fileDescriptor, (err) => {
                if (err) {
                  callbakc('Error closing new file')
                } else {
                  callback(false)
                }
              })
            } else {
              callback('Error writing to new file')
            }
          },
        )
      } else {
        callback('Could not create new file, it may already exist')
      }
    }
  )
}

lib.read = (dir, file, callback) => {
  fs.readFile(
    `${root}${dir}/${file}.json`,'utf8',
    function(err, data) {
      callback(err,data)
    },
  )
}

lib.update = (dir, file, data, callback ) => {
  fs.open(
    `${root}${dir}/${file}.json`,
    'r+',
    (err, fileDescriptor) => {
      if (!err && fileDescriptor) {
        let stringData = JSON.stringify(data)
        fs.ftruncate(fileDescriptor, err => {
          if (!err) {
            fs.writeFile(fileDescriptor, stringData, err => {
              if (!err) {
                fs.close(fileDescriptor, (err) => {
                  if (!err) {
                    callback(null, true)
                  } else {
                    callback('Error closing file')
                  }
                })
              } else {
                callback('Error writing to existing file')
              }
            })
          } else {
            callback('Error truncating file.')
          }
        })
      } else {
        callback('Couldn\'t open the file for updating, it may not exist yet.')
      }
    }
  )
}

lib.delete = (dir, file, callback) => {
  fs.unlink(
    `${root}${dir}/${file}.json`,
    err => {
      if (!err) {
        callback(null)
      } else {
        callback('Error deleting file')
      }
    },
  )
}

module.exports = lib
