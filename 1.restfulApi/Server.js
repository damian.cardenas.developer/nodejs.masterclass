// dependencies
const url = require('url')
const StringDecoder = require('string_decoder').StringDecoder

// handlers
const handlers = {}
handlers.ping = (data, callback) => {
  callback(200)
}
handlers.notFound = (data, callback) => {
  callback(404)
}

// router
const router = {
  ping: handlers.ping
}

const Server = (req, res) => {
  console.log('request started')
  // get URL and parse it
  let parsedUrl = url.parse(req.url, true)
  
  // get path
  let path = parsedUrl.pathname
  let trimmedPath = path.replace(/^\/+|\/+$/g, '')
  
  // get query string parameters
  const queryStringObject = parsedUrl.query

  // get the http method
  const method = req.method.toLowerCase()

  // get the headers
  const headers = req.headers

  // get the payload
  const decoder = new StringDecoder('utf-8')
  let buffer = ''
  req.on('data', data => {
    buffer += decoder.write(data)
  })

  req.on('end', () => {
    buffer += decoder.end()
    console.log(`${method} -> ${trimmedPath}`)
    console.log('query: ', queryStringObject)
    console.log('headers: ', headers)
    console.log('payload: ', buffer)
    
    // choose the handler
    const choosenHandler = typeof(router[trimmedPath]) !== 'undefined' 
      ? router[trimmedPath]
      : handlers.notFound
    
    // construct data object to send to the handler
    const data = {
      path: trimmedPath,
      query: queryStringObject,
      method,
      headers,
      payload: buffer,
    }
    
    choosenHandler(data, (statusCode, payload) => {
      statusCode = typeof(statusCode) == 'number' 
        ? statusCode
        : 200
      payload = typeof(payload) == 'object' ? payload : {}
      res.setHeader('Content-Type', 'application/json')
      res.writeHead(statusCode)
      res.end(JSON.stringify(payload))
      console.log(statusCode, payload)
    })
  })
}
module.exports = Server
