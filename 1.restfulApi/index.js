/*
 * Title: Restful api Exercise
 * Description: Entrypoint form the restful api
 * Author: Damian Cardenas <damian.cardenas.developer@gmail.com>
 * Date: 01/11/2021
 */

// Dependencies
const http = require('http')
const https = require('https')
const config = require('./config')
const serverFunction = require('./Server')
const fs = require('fs')

// Server code
const httpServer = http.createServer(serverFunction)

httpServer.listen(config.httpPort, () => {
  console.log(`runing app at localhost:${config.httpPort} in ${config.envName} mode`)
})

const httpsServerOptions = {
  key: fs.readFileSync('./https/key.pem'),
  cert: fs.readFileSync('./https/cert.pem'),
}

const httpsServer = https.createServer(httpsServerOptions, serverFunction)

httpsServer.listen(config.httpsPort, () => {
  console.log(`runing app at localhost:${config.httpsPort} in ${config.envName} mode`)
})
