# nodejs.masterclass

This repo contains all the exercises from the nodejs masterclass course
from [pirple.com][pirple.com],

This file also contains a binnacle about all my activities related to this course.
the content will be shown in descending order so, the last activity that
had done will be shown bellow this paragraph to avoid scrolling until the end of this file.

## 02/11/2021 Day 2:

- Started classes at 22:46


## 01/11/2021 Day 1:

- I decide to increase my skills and start learning node.js in depth and started
  the course today making everithing ready to learn from this course.
- Started class 1 16:30  UTC -6
- finished first 3 sections and 13 lessons from the  4th section at 19:27 UTC -6
